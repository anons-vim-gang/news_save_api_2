const express = require('express');
const app = express();
const port = 3000;

const url = require('url');
const fs = require('fs');

const scrape = require('website-scraper');
const PuppeteerPlugin = require('./website-scraper-puppeteer');
const SaveToExistingDirectoryPlugin = require('website-scraper-existing-directory');

const ua = require('./user-agent-strings.js');
const url_base = "192.168.1.84:8000/"
const directory_saved_websites = "saved_original"
const directory_saved_pdfs = "saved_pdf"

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

/* Main Api to save an article: HOSTNAME/save_article/[URL_TO_SITE] */
app.get('/save_article/*', async (req, res, next) => {
    const url = req.params[0];
    if (!url) {
        res.send("Please provide URL param API/URL_HERE");
        console.log("User did not enter URL Param!");
    } else {
        /* IDEA: have originals, reader, pdf */
        const url_dir = directory_saved_websites + '/' + get_filename(url);
        console.log(`checking url: ${url}`);
        /* After passing the check if anything was inputted -> check if already saved site */
        if (fs.existsSync(url_dir)) {
            console.log('Directory exists! Redirecting user');
            new_url = '/original/' + url + '/index.html';
            // credit where credit is due: https://flaviocopes.com/express-headers/
            // on client side with fetch API this helped: https://dmitripavlutin.com/javascript-fetch-async-await/
            if (req.header('Content-Type') == 'application/json') {
                // json to client
                console.log(new_url);
                res.json({'redirect_url': new_url});
            } else {
                res.redirect(new_url);
                console.log(`Redirected user to saved version`);
            }
        } else {
            console.log('Directory not found. Scraping Site.');
            const pdf_filename = directory_saved_pdfs + '/' + make_filename_pdf(url);
            console.log(pdf_filename);
            const scrape_result = await scrape_site(url, pdf_filename);
            new_url = '/original/' + url + '/index.html';
            if (req.header('Content-Type') == 'application/json') {
                res.json({'redirect_url': new_url});
            } else {
                res.redirect(new_url);
                console.log(`Redirected user to saved version`);
            }
        }
    }
});

/* original/ + host + pathname --> gives location of site */
app.get('/original/*', (req, res) => {
    const url_original = req.params[0];
    console.log("GET Request for Original " + url_original);
    if (!url_original) {
        res.send("Please provide URL param API/URL_HERE");
        console.log("User did not enter URL Param!");
    } else {
        // idk but for articles need to provide URL + index.html
        res.sendFile(__dirname + '/' + directory_saved_websites + '/' + get_filename(url_original));
    }
});

app.get('/pdf/*', (req, res) => {
    const url_pdf = req.params[0];
    console.log("GET Request for PDF " + url_pdf);
    if (!url_pdf) {
        res.send("Please provide URL param API/URL_HERE");
        console.log("User did not enter URL Param!");
    } else {
        const finished_fn = __dirname + '/' + directory_saved_pdfs + '/' + make_filename_pdf(url_pdf) + '.pdf';
        console.log(`PDF Request. Sending: ${finished_fn}`);
        res.sendFile(finished_fn);
    }
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});

/* returns filename => host + pathname, to functions */
function get_filename(url) {
    const newURL = new URL(url);
    return newURL.host + newURL.pathname;
}

function make_filename_pdf(url) {
    let filename_construct = get_filename(url);
    // credit to: https://stackoverflow.com/questions/8485027/javascript-url-safe-filename-safe-string
    return filename_construct.replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

async function scrape_site(url, filename_pdf) {
    const options = {
        urls: [url],
        directory: directory_saved_websites + '/',
        sources: [
            {selector: 'img', attr: 'src'},
            {selector: 'link[rel="stylesheet"]', attr: 'href'},
            {selector: 'script', attr: 'src'}
        ],
        request: {headers: {'User-Agent': ua.googlebot}},
        filenameGenerator: 'bySiteStructure',
        plugins: [
            new PuppeteerPlugin({
                launchOptions: {
                    headless: true,
                    executablePath: '/usr/bin/chromium-browser',
                },
                scrollToBottom: { timeout: 10000, viewportN: 122},
                blockNavigation: true, /* optional */
                filename_pdf: filename_pdf,
            }),
            new SaveToExistingDirectoryPlugin()
        ]
    };
    const result = await scrape(options);
    return 0;
}
