# News Save API 2.0

For clarification: I first started developing News Save API out of a personal need to read the news without a paywall, after the initial start of development I then found a module on NPM that included all the things, well almost all, that I needed from puppeteer, and so I used it. ([News Save API](../news_save_api/))

## Project's Goal

My goal is not to infringe anyones copyright, but rather to make the news freely available to researchers, students and many more, that deserve access despite their lack of capital. 

## History

### Idea and initial Version

In early 2020 I got the idea to make this API but lacked the time to develop it, then in late 2020 I started developing [News Save API 0](../) which I then tested in daily use. My initial thought was that I would be using this API on the go, to browse the news on my phone, on my laptop I already had [Paywall Bypasser](https://github/TODO_FIND_THIS) to read the news. The goal in development then was a fast version, which acts kind of like Paywall Bypasser. This API has really, a very simple codebase, since it's mostly [Google's Cloud Functions Tutorial]() and one function of my own writing, which essentially just downloads a given URL with the Googlebot User-Agent and sends it back to the enduser. This whole "beast" was then deployed on Google Cloud Functions. While this does work very well there are a few issues I continued having with it, one being, that the images wouldn't load from the website's domain which I downloaded through this API. The changed objectives can be found further down.

### Version 1.0

In the first version I switched from python to javascript, that's maybe why my codebase is not structured in a way one might expect. With that switch came a change in libraries, for this version I used puppeteer, which is a browser automation library, like selenium in python, that essentially spins up a chromium instance, renders the full javascript webpage, and downloads it. Another cool feature, which I actually found right there in the tutorial for the library, is that you can "Print to PDF" or in noob-speak, you can save the webpage as a pdf-file.

In this version already I found myself not only wanting to bypass a paywall but more so to archive these articles, or just to cache them as to not hit the newspapers' API all the time. That's why I not only aim to develop an API to bypass a paywall, but to also give an enduser access to older archived articles, or maybe an article got taken down, and then one would have access to a prior saved version...

### Version 2.0

Coming soon...